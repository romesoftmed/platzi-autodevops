var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "the api application" });
});

router.get("/status", function (req, res, next) {
  res.json({ title: "welcome to the api", status: "up" });
});

module.exports = router;
